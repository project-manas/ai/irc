import cv2
import numpy as np
# 90 80 160    200 255 255
#(50,170,110), (150,255,255)

#0, 100, 140    180,180,255

#24 100 10   100 150 255
#45 40 30   80 255 255
def detectColour(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    redl = cv2.inRange(hsv, (0, 100, 120), (10, 255, 255))
    redu = cv2.inRange(hsv, (160, 100, 100), (179, 255, 255))
    red = redl + redu
    blue = cv2.inRange(hsv, (90,180,160), (200,255,255))
    green = cv2.inRange(hsv, (45, 40, 30), (80, 255, 255))
    red_px = cv2.countNonZero(red)
    blue_px = cv2.countNonZero(blue)
    green_px = cv2.countNonZero(green)

    height, width, _ = image.shape
    pixels = height * width
    if (pixels / 100.0) * 30 <= blue_px:
        return "blue"
    elif (pixels / 100.0) * 30 <= red_px:
        return "red"
    elif (pixels / 100.0) * 30 <= green_px:
        return "green"
    return None

